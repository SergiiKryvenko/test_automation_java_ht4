package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class HomePage extends BasePage {

    @FindBy(css = "header[role='banner']")
    protected WebElement header;

    @FindBy(xpath = "//li[@id='gh-minicart-hover']//a[contains(@href,'cart.payments.ebay.com/sc/view')]")
    protected WebElement cartIcon;

    @FindBy(xpath = "//div[@role='navigation']//span[@class='gh-ug-guest']/a[contains(@href,'signin.ebay.com')]")
    protected WebElement signInButton;

    @FindBy(xpath = "//div[@role='navigation']//span[@class='gh-ug-guest']//a[contains(@href,'reg.ebay.com')]")
    protected WebElement registerButton;

    @FindBy(xpath = "//a[@href='https://www.ebay.com/']")
    protected WebElement logo;

    @FindBy(css = "input[type='text']")
    protected WebElement searchField;

    @FindBy(id = "gh-btn")
    protected WebElement searchButton;

    @FindBy(css = "#gh-cart-n")
    protected WebElement cartIconProductsCount;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void isHeaderVisible() {
        header.isDisplayed();
    }

    public void isCartIconVisible() {
        cartIcon.isDisplayed();
    }

    public void isSignInButtonVisible() {
        signInButton.isDisplayed();
    }

    public void clickSignInButton() {
        signInButton.click();
    }

    public void isRegisterButtonVisible() {
        registerButton.isDisplayed();
    }

    public void isSearchFieldVisible() {
        searchField.isDisplayed();
    }

    public ShopingCart clickCartButton() {
        cartIcon.click();
        return new ShopingCart(driver);
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public SearchResultsPage enterTextToSearchFieldAndClickSearch(final String searchText) {
        logger.info("start Search text " + searchText);
        enterTextToSearchField(searchText);
        clickSearchButton();
        return new SearchResultsPage(driver);
    }

    public int getAmountOfProductsInCart() {
        wait.visibilityOfWebElement(cartIconProductsCount);
        int productsInCart = Integer.parseInt(cartIconProductsCount.getText());
        logger.info("quantity products in the Cart = " + productsInCart);
        return productsInCart;
    }

    @Step(value = "get URL address")
    public String getHomeURL() {
        String currentURL = driver.getCurrentUrl();
        logger.info("current URL = " + currentURL);
        return currentURL;
    }

}