package com.epam.external.courses.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends HomePage {

    @FindBy(css = "div[class*='box-marpad'] a#binBtn_btn")
    private WebElement buyItNowButton;

    @FindBy(xpath = "//div[contains(@class,'box-marpad')]//a[@id='isCartBtn_btn']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div[contains(@class,'quantity')]//input")
    private WebElement quantityField;

    @FindBy(xpath = "//div[@id='watchWrapperId']//span[text()='Add to Watchlist']")
    private WebElement addToWatchlistButton;

    @FindBy(css = "div.nonActPanel div#vi-itm-cond")
    private WebElement conditionField;

    @FindBy(css = "div#streamline-bin-layer button#sbin-signin-btn")
    private WebElement gusetPopUpSignIn;

    @FindBy(css = "div#streamline-bin-layer button#sbin-gxo-btn")
    private WebElement gusetPopUpCheckOut;

    @FindBy(id = "prcIsum")
    private WebElement price;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public ShopingCart clickToAddToCartButton() {
        logger.info("Product page: click 'Add To Cart' button");
        addToCartButton.click();
        return new ShopingCart(driver);
    }

    public CheckOutPurchase clickToBuyItNowButton() {
        buyItNowButton.click();
        return new CheckOutPurchase(driver);
    }

    public String getConditionOfProduct() {
        return conditionField.getText();
    }

    public ProductPage enterQuantityOfProducts(int quantity) {
        quantityField.clear();
        logger.info("Product page: entered quantity of products = " + quantity);
        quantityField.sendKeys(String.valueOf(quantity));
        return this;
    }

}
