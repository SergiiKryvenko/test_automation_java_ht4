package com.epam.external.courses.ui.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultsPage extends HomePage {

    @FindBy(xpath = "//div[contains(@class,'srp-river-results')]/ul/li[contains(@class,'s-item')]//h3/..")
    private List<WebElement> searchListIcon;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    private void clickSearchListOnFirstProduct() {
        searchListIcon.get(0).click();
    }

    public ProductPage clickToFirstProduct() {
        logger.info("Seqrch page: click to first product");
        clickSearchListOnFirstProduct();
        return new ProductPage(driver);
    }
}
