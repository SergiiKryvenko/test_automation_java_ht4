package com.epam.external.courses.ui.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class SmokeTest extends TestRunner {
    
    @DataProvider(name = "Data-Provider-CheckHomeURL")
    public Object[][] parameterForCheckHomeURL() {
        return new Object[][] { { "https://www.ebay.com/" } };
    }

    @DataProvider(name = "Data-Provider-CheckSearchPage")
    public Object[][] parameterForCheckSearchPage() {
        return new Object[][] { { "garmin", "https://www.ebay.com/sch/" } };
    }

    @DataProvider(name = "Data-Provider-CheckQuantityInCart")
    public Object[][] parameterForCheckQuantityInCart() {
        return new Object[][] { { "garmin", 5, 5 } };
    }

    @Epic("A test URL Ebay home page")
    @Feature(value = "check a URL address on the home page")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Load Ebay home page and check the URL.")
    @Story(value = "Check URL.")
    @Test(dataProvider = "Data-Provider-CheckHomeURL")
    public void checkURLHomePage(String expected) {
        String actual = getHomePage().getHomeURL();
        assertEquals(actual, expected, "the actual URL didn't match the " + expected);
    }

    @Test(dataProvider = "Data-Provider-CheckSearchPage")
    public void checkSearchPage(String searchProduct, String expected) {
        String actual = getHomePage()
                .enterTextToSearchFieldAndClickSearch(searchProduct)
                .getHomeURL();
        assertTrue(actual.contains(expected), "the actual URL didn't match the " + expected);
    }
    
    @Test(dataProvider = "Data-Provider-CheckQuantityInCart")
    public void checkQuantityInCart(String searchProduct, int quantity, int expected){
        int actual = getHomePage()
                .enterTextToSearchFieldAndClickSearch(searchProduct)
                .clickToFirstProduct()
                .enterQuantityOfProducts(quantity)
                .clickToAddToCartButton()
                .getAmountOfProductsInCart();
        assertEquals(actual, expected, "the actual quantity didn't match the " + expected);
    }

}
