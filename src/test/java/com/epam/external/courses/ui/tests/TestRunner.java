package com.epam.external.courses.ui.tests;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.epam.external.courses.ui.pages.HomePage;
import com.epam.external.courses.ui.utils.CapabilityFactory;

import io.qameta.allure.Step;

public abstract class TestRunner {
	private final Long ONE_SECOND_DELAY = 1000L;
	//
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	//
	protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
	private CapabilityFactory capabilityFactory = new CapabilityFactory();
	//
	private static final String URL = "https://www.ebay.com/";

	@AfterClass(alwaysRun = true)
	public void tearDownAfterClass() throws Exception {
	    logger.debug("start tearDownAfterClass()");
        logger.trace("lunch tearDownAfterClass()");
        logger.info("lunch tearDownAfterClass()");
//	    presentationSleep(1);
		if (driver != null) {
		    driver.remove();
		}
	}

	@BeforeMethod
	@Parameters(value = {"browser"})
	public void setUp(String browser) throws MalformedURLException {
	    logger.debug("start setUp() with a browser " + browser);
        logger.trace("lunch setUp() with a browser " + browser);
        logger.info("lunch setUp() with a browser " + browser);
	    driver.set(new RemoteWebDriver(new URL("http://192.168.136.1:4444/wd/hub"), capabilityFactory.getCapabilities(browser)));
	    getDriver().get(URL);
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
	    logger.debug("start tearDown()");
        logger.trace("lunch tearDown()");
        logger.info("lunch tearDown()");
		if (!result.isSuccess()) {
		    logger.warn("Test " + result.getName() + " ERROR");
			// Take Screenshot, save sourceCode, save to log, prepare report, Return to previous state, logout, etc.
		}
		// logout, get(urlLogout), delete cookie, delete cache
		getDriver().close();
	}
	
	protected void presentationSleep() {
		presentationSleep(1);
	}
	
	protected void presentationSleep(int seconds) {
		try {
			Thread.sleep(seconds * ONE_SECOND_DELAY); // For Presentation ONLY
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public WebDriver getDriver() {
        return driver.get();
    }
	
	@Step(value = "load Home page")
	protected HomePage getHomePage() {
	    return new HomePage(getDriver());
	}
}